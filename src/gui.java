import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class gui extends JFrame {
	JPanel panel;
	JButton enter;
	JButton brows;
	JLabel label;
	JTextArea regular;
	JTextArea text;
	JTextArea found;
	
	public gui(){
		panel = new JPanel();
		
		label = new JLabel("���������� ��������� ->");
		
		regular = new JTextArea(1, 45);
		JScrollPane scroller = new JScrollPane(regular);
		//regular.setLineWrap(true); //�������� ������� ������
		scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		
		enter = new JButton("����");
		enter.addActionListener(new EnterListener());
		
		text = new JTextArea(30, 33);
		JScrollPane scroller1 = new JScrollPane(text);
		text.setLineWrap(true); //�������� ������� ������
		scroller1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scroller1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		found = new JTextArea(30, 33);
		JScrollPane scroller2 = new JScrollPane(found);
		found.setLineWrap(true); //�������� ������� ������
		scroller2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scroller2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		brows = new JButton("������� �����");
		brows.addActionListener(new BrowsListener());
		
		panel.add(label);
		panel.add(scroller);
		panel.add(enter);
		panel.add(scroller1);
		panel.add(scroller2);
		panel.add(brows);
		

		this.add(panel);
		this.setSize(800, 600);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	class EnterListener implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {
			//String s;
			Regular r = new Regular();
			//s=regular.getText();
			r.setPattern(regular.getText());
			
			found.setText(r.getFound(text.getText()));
		}
	}
	class BrowsListener implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {			
			text.setText(new OpenFile().openFile(new OpenFile().chooseFile()));
	       // File[] file = dialog.getSelectedFiles();
			//new Regular().openFile();
			
		}
	}
}
