import java.io.*;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class OpenFile {
	public File[] chooseFile(){
		JFileChooser dialog = new JFileChooser();
        dialog.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        dialog.setApproveButtonText("�������");//������� �������� ��� ������ ��������
        dialog.setDialogTitle("�������� ���� ��� ��������");// ������� ��������
        dialog.setDialogType(JFileChooser.OPEN_DIALOG);// ������� ��� ������� Open ��� Save
        dialog.setMultiSelectionEnabled(true); // ��������� ����� ��������� ������
        
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT file", "txt");//An implementation of FileFilter that filters using a specified set of extensions
		dialog.setFileFilter(filter);
		
        dialog.showOpenDialog(dialog);
        File[] file = dialog.getSelectedFiles();
        return file;
	}
	public String openFile(File[] files){
		//File myFile = new File("MyFile.txt");
		String str = "";
		try {
			FileReader fileReader = new FileReader(files[0]);
			
			BufferedReader reader = new BufferedReader(fileReader);
			String line = null;
			while ((line = reader.readLine())!=null){
				//line+="\n";
				str+=line+"\n";
				//System.out.println(1);
			}
			reader.close();
		} catch(Exception ex) {
			System.out.println("���� �� ������");
		}
		return str;
	}
		

}
